class Post {
    constructor(parentSelector, title, img) {
        this.title = title;
        this.parentSelector = parentSelector;
        this.img = img;
        this.date = new Date();
        this.render();
    } 
    toString() {
       return JSON.stringify({
           title: this.title,
           date: this.date.toJSON(),
           img: this.img,
        })
    }
    getUppercaseTitle() {
        return this.title.toUpperCase()
    }
    render() {
        this.container = document.querySelector(this.parentSelector);
        this.containerInner = document.createElement('div');
        this.postImage = document.createElement('img');
        this.postTitle = document.createElement('p');
        this.postDate = document.createElement('p');
        this.postTitle.textContent = this.title;
        this.postDate.textContent = this.date.toJSON();

        this.postImage.src = this.img;
        this.postImage.classList.add('post-image');
        this.postImage.alt = 'webpack';
        this.container.append(this.containerInner);
        this.containerInner.append(this.postTitle, this.postDate, this.postImage);
    }
}

export default Post