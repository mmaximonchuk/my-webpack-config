import Post from "./Post";
import json from "./assets/json.json";
import './styles/styles.css';
import WebpackLogo from './assets/webpack-logo.png';

const post = new Post('.container', "Webpack Post Title", WebpackLogo);

console.log('JSON:', json);
console.log('JSON:', post);